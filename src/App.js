import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import fetchJsonp from "fetch-jsonp";

//Imported components from react-bootstrap
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image'

class App extends Component {
    
    //Bind all events and set the state in the constructor
    constructor(props){
        super(props);
        this.getMP = this.getMP.bind(this);
        this.changeInput = this.changeInput.bind(this);
        this.state = {
            postalCode : "",
            text : "",
            image : "",
            party : "",
            district : "",
            url : "",
            valid : true
        };
    }
    
    //Event when the "Go" button is pressed. Retrieves a MP and their information.
    getMP(){
        //Append the Postal Code to the base URL and retrieve the JSON file
        const url = "https://represent.opennorth.ca/postcodes/" + this.state.postalCode.toUpperCase().replace(/\s/g,'');
        console.log(url);
        fetchJsonp(url)
        .then(resp => resp.json())
        .then(data => {
            //Filter the JSON for the MP
            const representative = data.representatives_centroid.filter(
                function (centroid) {return centroid.elected_office === "MP"}
            );
            //Get the info of the the MP    
            this.setState({text: representative[0].name});
            this.setState({image: representative[0].photo_url});
            this.setState({party: representative[0].party_name});
            this.setState({district: representative[0].district_name});
            this.setState({url: representative[0].url});
            this.setState({valid: true});
            
        })
        //If there are no JSON file due to a bad entry, return an error.
        .catch(error =>{
            this.setState({text: "Sorry, this postal code does not exist"});
            this.setState({image: ""});
            this.setState({party: ""});
            this.setState({district: ""});
            this.setState({url: ""});
            this.setState({valid: false});
        });
    }
    
    //Change the text of the input box when the user enters something
    changeInput(e){
        this.setState({postalCode: e.target.value});
    }
    
  render() {
    let isValid = this.state.valid;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React Tech Challenge</h1>
        </header>
        {/* Used a container to better sort the page.*/}
        <Container className = "Container">
        <Row >
        <Col>
            <p className="App-intro">
              <br />
              Find your Member of Parliament
              <br />
              <input placeholder = "Postal Code" 
                     onChange = {this.changeInput}/>
              <Button variant = "primary" onClick = {this.getMP}>
                Go
              </Button>
            </p>
        </Col>
        <Col xs = {7}>
            <br />
            <Container className = "Container">
            <Row >
            <Col>
                <Image src={this.state.image} fluid />
            </Col>
            {/* Made the column bigger to push the info closer to the picture. */}
            <Col xs = {9}>
                <br />
                {/* If the user entered a valid Postal Code, it will display the MP's info.
                    If the user entered a valid Postal Code and a MP was returned, 
                    a link to their profile can be accessed by clicking on their name.
                    If the input is not valid, it will tell the user there is something wrong with the input.
                */}
                {isValid ? (
                    <Button variant = "link" className = "MP-Link" onClick={()=> window.open(this.state.url, "_blank")}>
                        {this.state.text}
                    </Button>
                    ) : (
                    <h1 className = "MP-Link">{this.state.text}</h1>
                )}
                <br />
                <br />
                {this.state.party}
                <br />
                {this.state.district}
            </Col>
            </Row>
            </Container>
        </Col>
        </Row>
        </Container>
      </div>
    );
  }
}

export default App;
